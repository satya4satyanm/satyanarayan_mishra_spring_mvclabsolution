<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Student Debate App</title>
</head>
<body>
	<div align="center">
		<h1>Students List</h1>
		<h3>
			<a href="newStudent">Add Student</a>
		</h3>
		<table border="1">
			<tbody>
				<tr>
					<th>Student Id</th>
					<th>Name</th>
					<th>Department</th>
					<th>Country</th>
				</tr>
				<c:forEach var="student" items="${students}">
					<tr>

						<td>${student.id}</td>
						<td>${student.name}</td>
						<td>${student.department}</td>
						<td>${student.country}</td>
						<td><a href="updateStudent?id=${student.id}">Update</a> <a
							href="deleteStudent?id=${student.id}">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>